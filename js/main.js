import {
  layThongTinTuFom,
  renderThongTinCV,
  sortAZ,
  sortZA,
} from "./controllers/controlCongViec.js";
import { CongViec, CongViecHT } from "./models/modelCongViec.js";

let congViecArr = [];
let congViecArrHT = [];

//add thong tin cong viec vao Arr
document.getElementById("addItem").addEventListener("click", () => {
  let newCongViec = layThongTinTuFom();
  if (newCongViec == "") {
    return;
  }
  let congViecOb = new CongViec(newCongViec, 0);
  congViecArr.push(congViecOb);
  renderThongTinCV(congViecArr, "todo");
});

//click remove CV
let removeCV = (index, tienDoCV) => {
  if (tienDoCV) {
    congViecArrHT.splice(index, 1);
    renderThongTinCV(congViecArrHT, "completed");
  } else {
    congViecArr.splice(index, 1);
    renderThongTinCV(congViecArr, "todo");
  }
};
window.removeCV = removeCV;
//click hoan thanh CV
let completeCV = (index) => {
  let congViecEdit = congViecArr[index];
  congViecArr.splice(index, 1);
  renderThongTinCV(congViecArr, "todo");
  let congViecHT = new CongViecHT(congViecEdit.tenCV, 1);
  congViecArrHT.push(congViecHT);
  renderThongTinCV(congViecArrHT, "completed");
  document.querySelector(".fa-check-circle").disabled = true;
};
window.completeCV = completeCV;

//sort a-z
document.getElementById("two").addEventListener("click", () => {
  sortAZ(congViecArr);
  renderThongTinCV(congViecArr, "todo");
  sortAZ(congViecArrHT);
  renderThongTinCV(congViecArrHT, "completed");
});
//sort a-z
document.getElementById("three").addEventListener("click", () => {
  sortZA(congViecArr);
  renderThongTinCV(congViecArr, "todo");
  sortZA(congViecArrHT);
  renderThongTinCV(congViecArrHT, "completed");
});
