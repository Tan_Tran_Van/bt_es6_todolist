//lay thong tin cong viec

export let layThongTinTuFom = () => {
  let tenCV = document.getElementById("newTask").value.trim();
  return tenCV;
};

//render cong viec len form
export let renderThongTinCV = (danhSachCV, idAdd) => {
  let contentHTML = "";
  danhSachCV.forEach((item, index) => {
    contentHTML += `<li>
        <span>${item.tenCV}</span>
        <span class="buttons">
        <button class="remove" onclick="removeCV(${index},${item.tienDoCV})"><i class="fa fa-trash" ></i></button>
        <button class="complete " onclick="completeCV(${index})"><i class="fa fa-check" ></i></button>
        <button class="complete " ><i class="fa fa-check-circle"></i></button>
        </span>
        </li>`;
  });
  document.getElementById(idAdd).innerHTML = contentHTML;
};

//sort a-z
export let sortAZ = (congViec) => {
  congViec.sort((a, b) => a.tenCV.localeCompare(b.tenCV));
};
export let sortZA = (congViec) => {
  congViec.sort((a, b) => -1 * a.tenCV.localeCompare(b.tenCV));
};
