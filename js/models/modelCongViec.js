export class CongViec {
  constructor(tenCV, tienDoCV) {
    this.tenCV = tenCV;
    this.tienDoCV = tienDoCV;
  }
}

export class CongViecHT extends CongViec {
  constructor(tenCV, tienDoCV) {
    super(tenCV, tienDoCV);
    this.tienDoCV = tienDoCV;
  }
}
